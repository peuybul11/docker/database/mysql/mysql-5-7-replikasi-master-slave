# MySQL 5.7 Replikasi Master Slave

Ubah default password pada file `mysql-master/database.env` dan `mysql-slave/database.env` .
Pastikan master dan slave memiliki port dan server-id yang berbeda.

Jalankan Mysql Master
```
cd mysql-master ; docker-compose up -d --build
```
```
cd mysql-slave ; docker-compose up -d --build
```


## Tahap Replikasi 

# 1. Master Host
Masuk ke kontainer mysql master
```
docker exec -it container-mysql-master bash
```

Masuk ke database
```
mysql -u root -p
```

Buat User untuk haproxy (load balance)
```
CREATE USER 'haproxy_user'@'%';
```

Buat Super User
```
CREATE USER 'mysql_user'@'%' IDENTIFIED BY 'password';
```
```
GRANT ALL PRIVILEGES ON *.* TO 'mysql_user'@'%' WITH GRANT OPTION;
```

Buat User untuk replikasi
```
CREATE USER 'replication_user'@'%' IDENTIFIED BY 'replication_password';
```
```
GRANT REPLICATION SLAVE ON *.* TO 'replication_user'@'%';
```

```
FLUSH PRIVILEGES;
```
```
SELECT user, host FROM mysql.user;
```

Periksa Status
```
SHOW MASTER STATUS\G
```


# 2. Slave Host
Masuk ke kontainer mysql slave
```
docker exec -it container-mysql-slave bash
```

Masuk ke database
```
mysql -u root -p
```

Buat User untuk haproxy (load balance)
```
CREATE USER 'haproxy_user'@'%';
```

Buat User Read Only Akses
```
CREATE USER 'mysql_user'@'%' IDENTIFIED BY 'password';
```
Berikan akses Read Only pada mysql_user
```
GRANT SELECT ON *.* TO 'mysql_user'@'%';
```
```
FLUSH PRIVILEGES;
```

Tahap replikasi ke Master
Stop Replikasi Mode
```
STOP SLAVE;
```

Lalu jalankan
```
CHANGE MASTER TO
MASTER_HOST='IP_MYSQL_MASTER',
MASTER_USER='USER_REPLIKASI',
MASTER_PASSWORD='PASSWORD_REPLIKASI',
MASTER_LOG_FILE='LOG_BIN_FILE',
MASTER_LOG_POS=LOG_POS,
MASTER_PORT=PORT_MYSQL_MASTER;
```

Contoh
```
CHANGE MASTER TO
MASTER_HOST='192.168.1.10',
MASTER_USER='replication_user',
MASTER_PASSWORD='replication_password',
MASTER_LOG_FILE='mysql-bin.000001',
MASTER_LOG_POS=1234,
MASTER_PORT=3306;
```

Pastikan Anda menggunakan alamat IP, nama user, dan kata sandi yang benar. Nama dan posisi file log harus sama dengan nilai yang Anda peroleh dari server master.

Start Replikasi Mode
```
START SLAVE;
```


## Jika slave gagal replika ke master, coba cek status nya dengan
Periksa status replikasi
```
SHOW SLAVE STATUS\G
```

jika dirasa semua config sudah benar maka coba **restart container** slave lalu coba periksa kembali replikasi nya.